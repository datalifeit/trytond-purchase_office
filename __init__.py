# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import purchase
from . import invoice
from . import party
from . import product


def register():
    Pool.register(
        party.PartyOffice,
        purchase.Purchase,
        purchase.PurchaseLine,
        invoice.InvoiceLine,
        module='purchase_office', type_='model')
    Pool.register(
        purchase.Purchase2,
        purchase.PurchaseLine2,
        module='purchase_office', type_='model',
        depends=['account_office'])
    Pool.register(
        product.TemplateOffice,
        purchase.PurchaseLine4,
        module='purchase_office', type_='model',
        depends=['office_product'])
    Pool.register(
        purchase.Purchase3,
        module='purchase_office', type_='model',
        depends=['office_sequence'])
    Pool.register(
        purchase.PurchaseLine3,
        module='purchase_office', type_='model',
        depends=['office_account_product', 'account_office'])
