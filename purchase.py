# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.modules.company_office.office import OfficeMixin
from trytond.transaction import Transaction
from trytond.pyson import Eval, If, Bool


class Purchase(OfficeMixin, metaclass=PoolMeta):
    __name__ = 'purchase.purchase'


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    office = fields.Function(
        fields.Many2One('company.office', 'Office'),
        'get_office', searcher='search_office')

    @fields.depends('purchase', '_parent_purchase.office')
    def on_change_with_office(self):
        if self.purchase and self.purchase.office:
            return self.purchase.office.id

    def get_office(self, name=None):
        if self.purchase:
            return self.purchase.office.id if self.purchase.office else None

    @classmethod
    def search_office(cls, name, clause):
        return [
            ('purchase.%s' % clause[0], ) + tuple(clause[1:])
        ]


class Purchase2(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    def _get_invoice_purchase(self):
        Journal = Pool().get('account.journal')

        invoice = super()._get_invoice_purchase()

        if invoice:
            invoice.office = self.office
            if invoice.office and invoice.journal and \
                    invoice.office not in invoice.journal.offices:
                journals = Journal.search([
                    ('type', '=', invoice.journal.type)])
                for journal in journals:
                    if self.office in journal.offices:
                        invoice.journal = journal
                        break
        return invoice


class PurchaseLine2(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def get_invoice_line(self):
        lines = super().get_invoice_line()
        for line in lines:
            line.office = self.purchase.office
        return lines


class Purchase3(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @classmethod
    def set_number(cls, purchases):
        for purchase in purchases:
            with Transaction().set_context(office_sequence=purchase.office.id
                    if purchase.office else None):
                super().set_number([purchase])


class PurchaseLine3(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def get_invoice_line(self):
        if self.purchase.office:
            office = self.purchase.office.id
        else:
            office = Transaction().context.get('office')
        with Transaction().set_context(office=office):
            return super().get_invoice_line()


class PurchaseLine4(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.product.domain.append(If(Bool(Eval('office')),
            ['OR',
                ('offices', '=', None),
                ('offices', '=', Eval('office'))
            ],
            []))
        cls.product.depends.append('office')
