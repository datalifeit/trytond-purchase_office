datalife_purchase_office
========================

The purchase_office module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-purchase_office/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-purchase_office)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
